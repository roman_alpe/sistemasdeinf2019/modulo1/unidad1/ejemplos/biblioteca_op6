﻿-- Ejemplo de creación de una base de datos de veterinarios
-- Tiene 2 tablas
DROP DATABASE IF EXISTS b20190605_op6;
CREATE DATABASE b20190605_op6;

-- Seleccionar la base de datos
USE b20190605_op6;

/*
  creando la tabla clientes
*/
CREATE TABLE ejemplar(
  cod_ejem int,
  titulo varchar(100),
  PRIMARY KEY(cod_ejem) -- creando la clave
CONSTRAINT fksocioejem FOREIGN KEY(cod_soc)
REFERENCES socio(cod_socio),
);

CREATE TABLE socio(
  cod_socio int,
  nombre varchar(100),
  apell varchar(100),
  PRIMARY KEY(cod_socio) -- creando la clave
CONSTRAINT fksocioejem FOREIGN KEY(cod_ejem)
REFERENCES socio(cod_ejem),
);